# 
# kAnyRemote - KDE frontend for anyRemote - a wi-fi or bluetooth remote for your PC.
#
# Copyright (C) 2007-2023 Mikhail Fedotov <anyremote@mail.ru>
# 
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 
# 

EXTRA_DIST = config.guess config.rpath ABOUT-NLS \
             NEWS README COPYING INSTALL AUTHORS ChangeLog TODO \
             data kanyremote.desktop kanyremote.appdata.xml specs

appdatadir = $(datarootdir)/appdata
dist_appdata_DATA = kanyremote.appdata.xml

dist_bin_SCRIPTS = kanyremote

SUBDIRS = po

ACLOCAL_AMFLAGS = -I m4

install-data-local:
	test -z $(DESTDIR)$(datadir)/pixmaps || mkdir -p -- . $(DESTDIR)$(datadir)/pixmaps;chmod 755 $(DESTDIR)$(datadir)/pixmaps
	test -z $(DESTDIR)$(datadir)/doc/kanyremote || mkdir -p -- . $(DESTDIR)$(datadir)/doc/kanyremote
	cp -r ChangeLog README COPYING AUTHORS $(DESTDIR)$(datadir)/doc/kanyremote/
	cp -r data/kanyremote_flash.png data/kanyremote_light.png data/kanyremote_off.png data/kanyremote_small.png data/kanyremote_logo.svg data/kanyremote.png $(DESTDIR)$(datadir)/pixmaps/
	chmod -R 644 $(DESTDIR)$(datadir)/pixmaps/kanyremote_small.png $(DESTDIR)$(datadir)/pixmaps/kanyremote.png $(DESTDIR)$(datadir)/pixmaps/kanyremote_flash.png $(DESTDIR)$(datadir)/pixmaps/kanyremote_off.png $(DESTDIR)$(datadir)/pixmaps/kanyremote_light.png $(DESTDIR)$(datadir)/pixmaps/kanyremote_logo.svg
	test -z $(DESTDIR)$(datadir)/applications || mkdir -p -- . $(DESTDIR)$(datadir)/applications;chmod 755 $(DESTDIR)$(datadir)/applications
	cp kanyremote.desktop $(DESTDIR)$(datadir)/applications/kanyremote.desktop
	chmod 644 $(DESTDIR)$(datadir)/applications/kanyremote.desktop
	
uninstall-local:
	rm -rf $(DESTDIR)$(datadir)/pixmaps/kanyremote.png \
	       $(DESTDIR)$(datadir)/pixmaps/kanyremote_flash.png \
	       $(DESTDIR)$(datadir)/pixmaps/kanyremote_off.png \
	       $(DESTDIR)$(datadir)/pixmaps/kanyremote_light.png \
	       $(DESTDIR)$(datadir)/pixmaps/kanyremote_small.png \
	       $(DESTDIR)$(datadir)/pixmaps/kanyremote_logo.svg \
           $(DESTDIR)$(datadir)/applications/kanyremote.desktop \
	       $(DESTDIR)$(datadir)/doc/kanyremote

check-gettext:
	@if test x$(USE_NLS) != "xyes" ; then echo "Missing gettext. Rerun configure and check for" \
	"'checking whether to use NLS... yes'!" ; exit 1 ; fi

pot:
	xgettext --language=Python --keyword=_tr --output=po/kanyremote.pot kanyremote

update-po: check-gettext
	@echo kanyremote > $(srcdir)/po/POTFILES.in.2 ; \
	if diff $(srcdir)/po/POTFILES.in $(srcdir)/po/POTFILES.in.2 >/dev/null 2>&1 ; then \
		rm -f $(srcdir)/po/POTFILES.in.2 ; \
	else \
		mv $(srcdir)/po/POTFILES.in.2 $(srcdir)/po/POTFILES.in ; \
	fi
	cd po && $(MAKE) $(AM_MAKEFLAGS) update-po

update-gmo: check-gettext
	cd po && $(MAKE) $(AM_MAKEFLAGS) update-gmo

force-update-gmo: check-gettext
	touch po/*.po
	cd po && $(MAKE) $(AM_MAKEFLAGS) update-gmo

.PHONY: check-gettext update-po update-gmo force-update-gmo
